@extends('layouts.app')

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                	@livewire('contact-live')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
