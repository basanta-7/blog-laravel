@if($paginator->hasPages())
<ul class="d-flex justify-content-between" style="padding: 0;">
	@if($paginator->onFirstPage())
		<li class="btn btn-light" disabled>
			Prev
		</li>
	@else	
		<li class="btn btn-secondary" wire:click="previousPage">
			Prev
		</li>
	@endif

	@if($paginator->hasMorePages())
	<li class="btn btn-secondary" wire:click="nextPage">
		Next
	</li>
	@else
	<li class="btn btn-light" disabled>
		Next
	</li>
	@endif
</ul>
@else
@endif
