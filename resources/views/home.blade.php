@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                    <p>this is an combined branch</p>
                    <div class="alert alert-danger">
                        Welcome to the master <strong style="text-transform: uppercase;">{{Auth::user()->name}}</strong>
                    </div>
                    <div class="alert alert-success">
                        Welcome to the experimental branch <strong style="text-transform: uppercase;">{{Auth::user()->name}}</strong>
                    </div>

                    <div class="alert alert-warning">
                        this is the warning experimental
                    </div>
                    <span class="badge badge-danger">Hello</span>
                    <span class="badge badge-success">Darkness</span>
                    <span class="badge badge-warning">My old friend</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
