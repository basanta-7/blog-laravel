<div class="container">
    <div class="row justify-content-center">
    	<div class="col-md-5">
    		<div class="card text-center">
    			<div class="card-body">
                    <form wire:submit.prevent="submit">
    		            <h4>User Register Form</h4>	
                        <div class="form-group mt-4">
                            <input type="text" placeholder="Full Name" wire:model="form.name" class="form-control">
                            @if ($errors->has('form.name')) <span class="text-danger">{{ $errors->first('form.name') }}</span> @endif
                        </div>  
                        <div class="form-group mt-4">
                            <input type="text" placeholder="Email" wire:model="form.email" class="form-control">
                            @if ($errors->has('form.email')) <span class="text-danger">{{ $errors->first('form.email') }}</span> @endif
                        </div>
                        <div class="form-group mt-4">
                            <input type="password" placeholder="Password" wire:model="form.password" class="form-control">
                            @if ($errors->has('form.password')) <span class="text-danger">{{ $errors->first('form.password') }}</span> @endif
                        </div>
                        <div class="form-group mt-4">
                            <input type="password" placeholder="Confirm Password" wire:model="form.password_confirmation" class="form-control">
                        </div>
                        <div class="form-group mt-4">
                            <button type="submit" class="btn btn-danger btn-block">Register</button>
                        </div>
                    </form>
    			</div>
    		</div>
    	</div>
    </div>
</div>