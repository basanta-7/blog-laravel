<div>
	@if($post)
	@if($isLoading)
		Loading...
	@else
	<h3>{{$post->title}}</h3>
	<p>
		{{$post->body}}
	</p>
	@endif
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Sorry!</strong> invalid input.<br><br>
            <ul style="list-style-type:none;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <section class="mb-3">
    	<img src="{{$image}}" alt="" style="object-fit: cover; max-height: 200px; max-width: 100%;" class="mb-3">
    	<input type="file" class="form-control" id="image" wire:change="$emit('fileChosen')">    
    </section>

	<form wire:submit.prevent="addComment">
		<div class="d-flex">
			<input type="email" class="form-control" wire:model.lazy="newEmail" placeholder="Email">
			<input type="text" class="form-control ml-2" wire:model.lazy="newName" placeholder="Name">
		</div>
		<div class="d-flex mt-3">
			<input type="text" placeholder="Comment" wire:model.lazy="newComment" class="form-control"> 
			<button class="btn btn-success ml-2">Add</button>
		</div>
	</form>
	<div class="mt-4">
		<h4>{{$comments->total()}} Comments</h4>
		@if($comments)
		@if($isLoading)
			Loading...
		@else
		@foreach($comments as $val)
		<div class="card mt-2">
			<div class="card-body d-flex">
				<div class="image-wrapper justify-content-between">
					@if($val->image)
					<img src="{{$val->imagePath}}" height="100px" width="100px;" class="img-fluid" alt="">
					@else
					<img src="{{asset('images/lall.jpg')}}" height="100px" width="100px;" class="img-fluid" alt="">
					@endif
				</div>
				<div class="val-body ml-3">
					<div class="d-flex justify-content-between">
						<h5 class="text-primary">{{$val->name}}</h5>
						<span role="button" class="pull-right"><i class="fa fa-times text-secondary" wire:click="remove({{$val->id}})"></i></span>
					</div>
					<small class="text-secondary"><i class="fa fa-clock"></i> {{$val->created_at->diffForHumans()}}</small>
					<p class="text-secondary">{{$val->comment}}</p>
				</div>
			</div>
		</div>
		@endforeach
		@endif
		<div class="mt-3 ml-0">
			{{$comments->links('cms-livewire.pagination-links')}}
		</div>
		@endif
	</div>
	@else
	<h2 class="text-center">Error 404 page not found</h2>
	@endif
</div>



