<div>
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Sorry!</strong> invalid input.<br><br>
            <ul style="list-style-type:none;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (Session::has('message'))
        <script>
            Swal.fire({
              position: 'top-end',
              icon: 'success',
              title: '{{Session::get('message')}}',
              showConfirmButton: false,
              timer: 3500
            })
        </script>
    @endif


    <div class="form-group">
        <form wire:submit.prevent="store">
            <h5>Add Contact Form</h5>
            <div class="d-flex">
                <input type="text" class="form-control" placeholder="Name" wire:model.lazy="name">
                <input type="email" class="form-control ml-3" placeholder="Email" wire:model.lazy="email">
            </div>
            <div class="row mt-3">
                <div class="col-md-6">
                    <select name="country" class="form-control" wire:model="country">
                        <option value="">--Select Country</option>
                        <option value="nepal">Nepal</option>
                        <option value="india">India</option>
                        <option value="china">China</option>
                        <option value="japan">Japan</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <input type="radio" name="gender" wire:model.lazy="gender" class="ml-3 mt-3" value="m"> Male
                    <input type="radio" name="gender" wire:model.lazy="gender" class="ml-3 mt-3" value="f"> Female
                </div>
            </div>
            <div class="mt-3">
                <label for="mesage">Message</label>
                <textarea name="message" rows="5" class="form-control" wire:model.lazy="message"></textarea>
            </div>
            <div class="mt-3">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Save</button>
                <button wire:click="resetValues" class="btn btn-danger"><i class="fa fa-trash"></i> Reset</button>
            </div>
        </form>
    </div>
    
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Message</th>
                <th>Gender</th>
                <th>Country</th>
                <th>Date</th>
                <th> <i class="fa fa-cog"></i> Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($contacts as $contact)
            <tr>
                <td>{{$contact->name}}</td>
                <td>{{$contact->email}}</td>
                <td>{{$contact->message}}</td>
                <td>{{$contact->gender == 'm' ? 'Male' : 'Female'}}</td>
                <td>{{$contact->country}}</td>
                <td>{{$contact->created_at->diffForHumans()}}</td>
                <td class="d-flex">
                    <a href="javascript:;" wire:click="confirmContactRemoval({{$contact->id}})" class="btn btn-sm btn-primary"><i class="fa fa-pen"></i></a>
                    <a class="btn btn-sm btn-danger ml-1" wire:click="confirmContactRemoval({{$contact->id}})" href="javascript:;"><i class="fa fa-trash"></i></a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
