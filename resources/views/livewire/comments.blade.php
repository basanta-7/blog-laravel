<div class="">
	<h4>Add a Comment</h4>
	@if (count($errors) > 0)
        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Sorry!</strong> invalid input.<br><br>
            <ul style="list-style-type:none;">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <section class="mb-3">
    	<img src="{{$image}}" alt="" style="object-fit: cover; height: 150px; max-width: 100%;" class="mb-3 rounded">
    	<input type="file" class="form-control" id="image" wire:change="$emit('fileChosen')">    
    </section>

	<form wire:submit.prevent="addComment">
		<div class="d-flex">
			<input type="email" class="form-control" wire:model.lazy="email" placeholder="Email">
			<input type="text" class="form-control ml-2" wire:model.lazy="name" placeholder="Name">
		</div>
		<div class="d-flex mt-3">
			<input type="text" placeholder="Comment" wire:model.lazy="comment" class="form-control"> 
			<button class="btn btn-success ml-2">Add</button>
		</div>
	</form>

	<div class="mt-5">
	<h4>Comments</h4>
	</div>

	@foreach($comments as $comment)
	<div class="card mt-4">
		<div class="card-body d-flex">
			<div class="image-wrapper justify-content-between">
				@if($comment->image)
				<img src="{{$comment->imagePath}}" height="100px" style="object-fit: cover;" width="100px;" class="rounded" alt="">
				@else
				<img src="{{asset('images/lall.jpg')}}" height="100px" width="100px;" class="rounded" alt="">
				@endif
			</div>
			<div class="val-body ml-3">
				<div class="d-flex justify-content-between">
					<h5 class="text-primary">{{$comment->name}}</h5>
					<span role="button" class="pull-right"><i class="fa fa-times text-secondary" wire:click="remove({{$comment->id}})"></i></span>
				</div>
				<small class="text-secondary"><i class="fa fa-clock"></i> {{$comment->created_at->diffForHumans()}}</small>
				<p class="text-secondary">{{$comment->comment}}</p>
			</div>
		</div>
	</div>
	@endforeach
	<div class="mt-3 ml-0">
		{{$comments->links('cms-livewire.pagination-links')}}
	</div>
</div>
