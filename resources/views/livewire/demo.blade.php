<div class="text-center">
	<a wire:click="increment" class="btn btn-primary">Add</a>
	@if($isLoading)
		<h1>loading...</h1>
	@else
	<h1>{{$count}}</h1>
	@foreach($data as $user)<p>{{$user->name}}</p>@endforeach
	@endif
	<a wire:click="decrement" class="btn btn-primary">Subtract</a>
</div>
