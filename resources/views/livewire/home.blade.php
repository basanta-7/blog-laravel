<div class="container">
    <div class="row justify-content-center">
    	<div class="col-md-6">
    		<div class="card">
    			<div class="card-body">
		            {{-- @livewire('post-live')			 --}}
		            @livewire('tickets')			
    			</div>
    		</div>
    	</div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    @livewire('comments')
                </div>
            </div>
        </div>
    </div>
</div>