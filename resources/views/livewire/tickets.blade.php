<div>
	<h3>Support Tickets ({{$tickets->count()}})</h3>
	<div class="mt-4">
		@if($tickets)
		@foreach($tickets as $ticket)
		<div class="ticket card mt-2 {{$active == $ticket->id ?  'bg-light' : ''}}" wire:click="$emit('ticketSelected', {{$ticket->id}})">
			<div class="card-body">
				<p class="bold">{{$loop->index+1}}. {{$ticket->question}}</p>
			</div>
		</div>
		@endforeach
		@else
			<h2 class="text-center">No tickets added</h2>
		@endif
	</div>
</div>
