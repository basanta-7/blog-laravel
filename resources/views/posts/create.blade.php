@extends('layouts.main')


@section('title','| Create New Post')
@section('content')

<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/select2/4.0.3/js/select2.min.js"></script>
</head>

	<br>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<h3>Create New Post</h3>
				<br>
					{!! Form::open(['route' => 'posts.store']) !!}
    					{{ Form::label('title','Title:')}}
    					{{ Form::text('title',null,array('class' => 'form-control')) }}

						{{ Form::label('slug','Slug :') }}
						{{ Form::text('slug', null, array('class' => 'form-control')) }}

						{{ Form::label('category_id','Category :') }}
						<select name="category_id" class="form-control">
						@foreach($categories as $category)	
							<option value='{{ $category->id }}'>{{ $category -> name }}</option>
						@endforeach
						</select>

						{{ Form::label('tags','Tags :') }}
						{{ Form::select('tags[]', $tags, null, ['class' => 'form-control select2', 'multiple' => 'multiple']) }}

    					{{ Form::label('body','Post Body:')}}
    					{{ Form::textarea('body',null,array('class' => 'form-control')) }}

						<br>

    					{{ Form::submit('Create Post',array('class' => 'btn btn-success btn-large btn-block'))}} 
					{!! Form::close() !!}
			</div>			
		</div>
		
		<script type="text/javascript">
			$(document).ready(function() {
				$('.select2-multi').select2();
			});
		</script>
@endsection