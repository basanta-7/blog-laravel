@extends('layouts.main')

@section('title','| View Post')

@section('content')
<br>
<div class="row">
	<div class="col-md-8">
		<h3>{{ $post->title}}</h3>
		<p class="lead"> 
			{{ $post->body}}
		</p>
		<hr>
		
		<div class="tags"><h5>Tags</h5>
			@foreach($post->tags as $tag)
				<span class="badge badge-info">{{ $tag->name}}</span>
			@endforeach
		</div>
	
		<div id="backend-comments" style="margin-top: 15px;">
			<h5>Comments <small>{{ $post->comments()->count() }}</small></h5>
			
			<table class="table">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Body</th>
					</tr>
				</thead>

				<tbody>
					@foreach($post->comments as $comment)
					<tr>
						<td>{{ $comment->name}}</td>	
						<td>{{ $comment->email}}</td>
						<td>{{ substr($comment->comment,0,100)}} {{ strlen($comment->comment)>100 ? "....." : "" }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-md-4">
		<div class="card" style="background-color: #f2f2f2;">
			<div class="card-body">
			<dl class="dl-horizontal">
				<label >Url :</label>
				<p><a href="{{ route('blog.single', $post->slug) }}"> {{ route('blog.single', $post->slug) }} </a></p>
			</dl>

			<dl class="dl-horizontal">
				<dt>Created At </dt>
				<dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
			</dl>

			<dl class="dl-horizontal">
				<dt>Last Updated </dt>
				<dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
			</dl>
			
			<dl class="dl-horizontal">
				<dt>Slug </dt>
				<dd>{{ $post->slug }}</dd>
			</dl>
			
			<dl class="dl-horizontal">
				<dt>Category </dt>
				<dd>{{ $post->category->name }}</dd>
			</dl>

			<hr>
			<div class="row">
				<div class="col-sm-6">
					{{ Html::linkRoute('posts.edit','Edit', array($post->id), array('class'=>'btn btn-primary btn-block'))}}
<!-- 					<a href="#" class="btn btn-primary btn-block">Edit</a>
 -->				</div>

				<div class="col-sm-6">
					{{ Form::open(['route'=> ['posts.destroy', $post->id], 'method'=>'DELETE']) }}
					
					{!! Form::submit('Delete',['class' => 'btn btn-danger btn-block']) !!}

					{!! Form::close() !!}
				</div>	
				<div class="col-md-12">
					<br>
					{{ Html::linkRoute('posts.index', '<< See All posts', [], ['class' => 'btn btn-success btn-block'])}}
				</div>
			</div>
			</div>
		</div>
	</div>
</div>

@endsection
