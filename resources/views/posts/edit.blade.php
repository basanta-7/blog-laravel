@extends('layouts.main')
@section('title','| Edit Blog Post')
@section('content')
{!! Html::style('css/select2.min.css') !!}

	<br>
<div class="row"> 
	<div class="col-md-8">
			<div class="form-group">
			<label >Title </label>{!! Form::model($post, ['route' => ['posts.update',$post->id],'method' =>'PUT']) !!}
			{{ Form::text('title',null, ['class'=> 'form-control']) }}
			<br>
			<label>Slug :</label>
			{{ Form::text('slug',null,['class' => 'form-control']) }}
			<br>
			{{ Form::label('category_id','Category :') }}
			{{ Form::select('category_id',$categories, $post->category->id, ['class' => 'form-control']) }}
			<br>
			{{ Form::label('tags','Tags :') }}
			{{ Form::select('tags[]', $tags, null, ['class' => 'form-control select2', 'multiple' => 'multiple']) }}
			<br>
			<label>Body</label>		
			{{ Form::textarea('body',null,['class'=>'form-control'])}}
			</div>
	</div>
	
	<div class="col-md-4">
		<div class="well">
			<dl class="dl-horizontal">
				<dt>Created At </dt>
				<dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
			</dl>

			<dl class="dl-horizontal">
				<dt>Last Updated </dt>
				<dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
			</dl>
			
			<hr>
			<div class="row">
				<div class="col-sm-6">
					{{ Html::linkRoute('posts.show','Cancel', array($post->id), array('class'=>'btn btn-danger btn-block'))}}
				</div>

				<div class="col-sm-6">
					{{ Form::submit('Save Changes', ['class'=>'btn btn-success btn-block'])}}

					<!-- {{ Html::linkRoute('posts.update','Save Changes', array($post->id), array('class'=>'btn btn-success btn-block'))}} -->
				</div>	
			</div>
		</div>
	</div>
			{!! Form::close() !!}

</div>
		<script>
			{!! Html::script('js/select2.min.js') !!}
		</script>
@endsection