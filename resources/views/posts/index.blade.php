@extends('layouts.main')

@section('title','| All Posts')

@section('content')
				<br>

	<div class="row">
		<div class="col-md-10">
			<h3>All Posts</h3>
		</div>

		<div class="col-md-2">
			<a href="{{ route('posts.create') }}" class="btn  btn-primary btn-block">Create New post</a>
		</div>

		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="breadcrumb">
				<form action="{{route('posts.index')}}" method="get">
					<div class="form-inline">
						<input type="text" class="form-control" id="post_search" placeholder="Search for blog" name="search" >
						<button type="submit">Search</button>										
					</div>
					<!-- <input type="hidden" id='postid' name="postid" readonly> -->
				</form>
			</div>
		</div>
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Title</th>
					<th>Body</th>
					<th>Created At</th>
					<th>Action</th>
				</thead>
				
				<tbody>
					@foreach ($posts as $post)
					<tr>
						<td>{{ $post->id}}</td>
						<td>{{ $post->title}}</td>
						<td>{{ substr($post->body, 0, 50) }} {{ strlen($post->body) > 50 ? "...." : " "}}</td>
						<td>{{ date('M j,Y',strtotime($post->created_at))}}</td>
						<td><a href="{{ route('posts.show',$post->id)}}" class="btn btn-success btn-sm">View</a> <a href="{{route('posts.edit',$post->id)}}" class="btn btn-primary btn-sm">Edit</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>

			<div class="text-center" style="margin-left: 500px;">
				{!! $posts -> links(); !!}
			</div>

		</div>
		<!-- <div class="col-md-3">
			<div class="cards" style="background-color:#fefefe; margin-top: -20px;">
				<div class="card-body">
					<div class="card-header">
    					Actions
 					</div>
					<ul class="list-group list-group-flush">
						<li class="list-group-item"><a href="{{ route('categories.index')}}">Categories</a></li>
					    <li class="list-group-item"><a href="{{ route('tags.index')}}">Tags</a></li>
					    <li class="list-group-item">Vestibulum at eros</li>
					</ul>
				</div>
			</div>
		</div> -->
	</div>

	<script>
	    // CSRF Token
	    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
	    $(document).ready(function(){

	      $( "#post_search" ).autocomplete({
	        source: function( request, response ) {
	          // Fetch data
	          $.ajax({
	            url:"{{route('search.posts')}}",
	            type: 'post',
	            dataType: "json",
	            data: {
	               _token: CSRF_TOKEN,
	               search: request.term
	            },
	            success: function( data ) {
	               response( data );
	            }
	          });
	        },
	        select: function (event, ui) {
	           // Set selection
	           $('#post_search').val(ui.item.label); // display the selected text
	           $('#postid').val(ui.item.value); // save selected id to input
	           return false;
	        }
	      });

	    });
	    </script>
@endsection