@extends('layouts.main')
@section('title','|Contact')
@section('content')

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <h4>Contact me</h4>
                <hr>
                <form action="{{ url('contact')}}" method='post'>
                {{ csrf_field()}}
                    <div class="form-group">
                        <label name="email">Email : </label>
                        <input type="email" name="email" class="form-control">
                    </div>
                    
                    <div class="form-group">
                        <label name="subject">Subject : </label>
                        <input type="text"class="form-control" name="subject">
                    </div>

                    <div class="form-group">
                        <label name="message">message : </label>
                        <textarea  name="message" class="form-control" placeholder="Type your message here"></textarea>
                    </div>
    
                    <input type="submit" value="Send message" class="btn btn-success">
                </form>
            </div>
        </div>
    
@endsection