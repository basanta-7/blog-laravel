@extends('layouts.main')

@section('title','|Homepage')
@section('content')

        <div class="row">
            <div class="col-md-12">
                <div class="jumbotron">
                        <h1 class="display-4">Welcome to My Blog! </h1>
                    <p class="lead">Thank you for visiting my blog. This is my test website built with laravel. Please read my latest   content.
                    </p>
                        <hr class="my-4">
                    <p class="lead">
                        <a class="btn btn-primary btn-lg" href="#" role="button">Popular Post</a>
                    </p>
                </div>
            </div>
        </div>
        <!--end of header row  -->
        <div class="row">
            <div class="col-md-8">
                @livewire('counter')
                @foreach($posts as $postlist)
                <div class="post">
                    <h4> {{ $postlist->title}} </h4>
                    <p>
                        {{ substr($postlist->body,0,260)}} {{ strlen($postlist->body) > 30 ? "...." : ""}}
                    </p>
                    <a href="{{ url('blog/'.$postlist->slug) }}" class="btn btn-primary">Read more</a>
                </div>
                <hr>
                @endforeach                
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 offset-md-4">
                {!! $posts -> links(); !!}
                
            </div>
        </div>
@endsection