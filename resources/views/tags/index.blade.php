@extends('layouts.main')

@section('title',' | Alll Tags')

@section('content')
<br>
	<div class="row">
		<div class="col-md-8">
			<h3>Tags</h3>
			<table class="table">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
					</tr>
				</thead>
				
				<tbody>
					@foreach($tags as $tag)
					<tr>
						<td>{{ $tag -> id}}</td>
						<td><a href="{{ route('tags.show',$tag->id)}}">{{ $tag -> name}}</a></td>
					</tr>
					@endforeach
				</tbody>

			</table>
		</div>

		<div class="col-md-4">
			<div class="card">
				<div class="card-body" style="background-color: #e0e0e0;">
					{!! Form::open(['route' => 'tags.store', 'method' => 'POST']) !!}
				
						<h4>New Tag</h4>
						{{ Form::label('name','Name') }}
						{{ Form::text('name',null,['class' => 'form-control']) }}
						<br>
						{{ Form::submit('Create New Tag', ['class' => 'btn btn-primary btn-block'])}}	
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
	
@endsection