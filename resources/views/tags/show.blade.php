@extends('layouts.main')

@section('title',"| $tag->name Tag")

@section('content')
	<br>
	<div class="row">
		<div class="col-md-8">	
			<h4>{{ $tag->name}} Tag <small style="font-size : 13px; color:gray;">{{$tag->posts()->count() }}Posts</small></h4>
		</div>
		
		<div class="col-md-4">
			<div class="col-md-12 offset-md-8">
				{{ Form::open(['route' => ['tags.destroy', $tag->id], 'method' => 'DELETE']) }}
					{{ Form::submit('Delete',['class' => 'btn btn-danger btn-sm'])}}
				{{ Form::close()}}
				<a href="{{ route('tags.edit',$tag->id)}}" class="btn btn-primary btn-sm" style="margin-top: -60px; margin-left: -50px;">Edit</a>
				
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>	
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Tags</th>
						<th>Action</th>
					</tr>
				</thead>

				<tbody>
					@foreach($tag->posts as $post)
					<tr>
						<td>{{ $post->id}} </td>
						<td>{{ $post->title}}</td>
						<td>@foreach($post->tags as $tag)
							<span class="badge badge-warning">{{ $tag->name}}</span>
						@endforeach
						</td>
						<td> <a href="{{route('posts.show',$post->id)}}" class="btn btn-primary btn-sm">View</a> </td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>	

@endsection