@extends('layouts.app')

@section('content')
<div class="container">
	<div class="card">
		<div class="p-4 d-flex justify-content-between">
			<h4>Lists of Posts</h4>
			<div class="btn btn-danger pull-right" onclick="getReport()">CSV</div>
		</div>
		<div class="card-body">
			<table class="table">
				<thead>
					<tr>
						<th>Title</th>
						<th>Slug</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script>
		$(document).ready(function(){
			var post_data = '';
  			fetch("{{config('app.url')}}/api/posts")
  			.then(response => response.json())
  			.then(data => {
  				$.each(data.data, function(key, value){
  					post_data += '<tr>';
  					post_data += '<td>'+value.title+'</td>';
  					post_data += '<td>'+value.slug+'</td>';
  					post_data += '</tr>';
  				});
  				$('.table').append(post_data);
  			});
		});

		const download = function(data){
			const blob = new Blob([data], {type: 'text/csv'});
			const url = window.URL.createObjectURL(blob);
			const a = document.createElement('a');
			a.setAttribute('hidden', '');
			a.setAttribute('href', url);
			a.setAttribute('download', 'download.csv');
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		}

		const objectToCsv = function(data){
			const csvRows = [];
			// get the headers			//
			const headers  = Object.keys(data[0]);
			csvRows.push(headers.join(','));

			// loop over the rows
			for(const row of data){
				const val = headers.map(header => {
					const value = row[header];
					const escaped = (''+row[header]).replace(/"/g, '\\"');
					return `"${escaped}"`;
				});
				csvRows.push(val.join(','));
			}
			return csvRows.join('\n');

		}

		const getReport = async function(){
			const jsonUrl = "{{config('app.url')}}/api/posts";
			const res = await fetch(jsonUrl);
			const json = await res.json();

			const data = json.data.map(row => ({
				title: row.title,
				slug: row.slug
			}));

			const csvData = objectToCsv(data);
			download(csvData);
		}
	</script>
@endsection