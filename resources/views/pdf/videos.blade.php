<html>
	<head>
		<title>Video Collection and video url lists</title>
	</head>
	<body>
		<h1>User List</h1>

		<table>
		  <thead>
		    <tr>
		      <th>#</th>
		      <th>Video Collection</th>
		      <th>Video URL</th>
		    </tr>
		  </thead>
		  <tbody>
		    @foreach($videos as $video)
		      <tr>
		        <td>1.</td>
		        <td>{{ $video->VideoCollection }}</td>
		        <td>{{ $video->VideoName }}</td>
		      </tr>
		    @endforeach
		  </tbody>
		</table>
	</body>
</html>