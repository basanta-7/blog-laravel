@extends('layouts.main')

@section('title', "| $post->title ")

@section('content')
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
<br>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<h4>{{ $post->title }}</h4>
			<p> {{ $post->body}}</p>
			<p><b>Posted in :</b> {{ $post-> category->name}}</p>
		</div>
	</div>
		<hr>

	<div class="row" style=" padding: 10px; margin-top:10px; ">
		<div class="col-md-8">
		<h4><i class="fas fa-comments"> {{ $post->comments()->count() }} Comments</i></h4>

			@foreach($post->comments as $comment)
				<div class="card" style="margin-top: 10px; ">
					<div class="card-body">
						<div class="image"><img src="{{ asset('images/lall.jpg') }}" alt="" class="rounded-circle" height="60px" width="60px" style="border : 3px solid #edeeef;"></div>
						<div class="commentbody" style="float: left; margin-top: -55px; margin-left: 100px;">
							<h5 class="card-title" style="">
							{{ $comment->name}}
							</h5>
							<p class="card-subtitle mb-2 text-muted">{{ date('M j, Y ', strtotime($comment->created_at)) }} </p>
							<p class="card-text">{{ $comment->comment}}</p>
						</div>
					</div>
				</div>
			@endforeach
		</div>	
	
		<div id="comment-form" class="col-md-4">
			<h5>Add Comment</h5>
			{{ Form::open(['route' => ['comments.store',$post->id], 'method' =>'POST']) }}
				<div class="row" >
					<div class="col-md-6">
						{{ Form::label('name',' Name') }}
						{{ Form::text('name',null,['class' =>'form-control'])}}
					</div>
					<div class="col-md-6">
						{{ Form::label('email',' Email') }}
						{{ Form::text('email',null,['class' =>'form-control'])}}
					</div>
					<div class="col-md-12">
						{{ Form::label('comment',' Comment') }}
						{{ Form::textarea('comment',null,['class' =>'form-control','rows' => '4'])}}
							<br>
						{{ Form::submit('Add Comment',['class' =>'btn btn-success btn-block'])}}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>


@endsection