@extends('layouts.main')

@section('title', ' | Blog')

@section('content')
<br>
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<h3>Blog</h3><br>
		</div>
	</div>

	@foreach($posts as $post)
	<div class="row">
		<div class="col-md-8 offset-md-2">
			<h4>{{ $post->title }}</h4>
			<h5>{{ date('M j, Y ', strtotime($post->created_at)) }}</h5>
			<p>{{ substr($post->body, 0, 250) }} {{ strlen($post->body) > 250 ? '....' : ""}}</p>

			<a href="{{ route('blog.single',$post->slug) }}" class="btn btn-primary">Read More</a>
			<hr>
		</div>	
	</div>
	@endforeach
	<div class="row">
		<div class="col-md-12">
			<div class="text-center" style="margin-left: 500px;">
				{!! $posts->links() !!}
			</div>
		</div>
	</div>
@endsection