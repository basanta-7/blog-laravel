-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 02, 2019 at 04:18 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'php(tutorials)', '2018-12-31 18:15:00', '2019-01-01 18:15:00'),
(2, 'ruby on rails', '2019-01-20 18:15:00', '2019-01-15 18:15:00'),
(3, 'Javascript', '2019-01-25 01:42:36', '2019-01-25 01:42:36'),
(4, 'SEO', '2019-01-25 01:43:10', '2019-01-25 01:43:10'),
(5, 'java', '2019-01-25 08:18:57', '2019-01-25 08:18:57'),
(6, 'java', '2019-01-28 00:06:40', '2019-01-28 00:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `name`, `email`, `comment`, `approved`, `post_id`, `created_at`, `updated_at`) VALUES
(1, 'Kamal', 'basantalfc@gmail.com', 'asdsdasdasds', 1, 7, '2019-01-27 01:49:39', '2019-01-27 01:49:39'),
(2, 'John Snow', 'johnsnow@gmail.com', 'this is nice post p.s. you suck', 1, 7, '2019-01-27 01:54:44', '2019-01-27 01:54:44'),
(3, 'David Villa', 'david@gmail.com', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit atque quisquam quibusdam doloremque non doloribus eaque at, ratione minus rerum distinctio tenetur totam natus, numquam nulla optio quo saepe aspernatur.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reprehenderit atque quisquam quibusdam doloremque non doloribus eaque at, ratione minus rerum distinctio tenetur totam natus, numquam nulla optio quo saepe aspernatur.', 1, 7, '2019-01-27 02:26:15', '2019-01-27 02:26:15'),
(4, 'John Cena', 'john@gmail.com', 'I\'ve got a nightmare to remember, I\'ll never be the same\r\nWhat began as laughter so soon would turn to pain\r\nThe sky was clear and frigid, the air was thick and still\r\nNow I\'m not one to soon forget and I bet I never will\r\nPicture for a moment the perfect irony\r\nA flawless new beginning eclipsed by tragedy\r\nThe uninvited stranger started dancing on his own\r\nSo we said goodbye to the glowing bride and made our way back home\r\nLife was so simple then\r\nWe were so innocent\r\nFather and mother\r\nholding each other\r\nWithout warning\r\nOut of nowhere\r\nLike a bullet\r\nFrom the night\r\nCrushing glass\r\nRubber and steel\r\nScorching fire\r\nGlowing lights\r\nScreams of terror\r\nPain of fear\r\nSounds of sirens\r\nSmoke in my eyes\r\nSudden stillness\r\nwrapped in silence\r\nNo more screaming\r\nNo more cries', 1, 7, '2019-01-27 02:52:16', '2019-01-27 02:52:16'),
(5, 'John petrucci', 'johnpet12@gmail.com', 'I\'ve got a nightmare to remember, I\'ll never be the same\r\nWhat began as laughter so soon would turn to pain\r\nThe sky was clear and frigid, the air was thick and still\r\nNow I\'m not one to soon forget and I bet I never will\r\nPicture for a moment the perfect irony\r\nA flawless new beginning eclipsed by tragedy\r\nThe uninvited stranger started dancing on his own\r\nSo we said goodbye to the glowing bride and made our way back home\r\nLife was so simple then\r\nWe were so innocent\r\nFather and mother\r\n', 1, 18, '2019-01-27 02:56:59', '2019-01-27 02:56:59'),
(6, 'James labrie', 'basantalfc@gmail.com', 'Now I\'m not one to soon forget and I bet I never will Picture for a moment the perfect irony A flawless new beginning eclipsed by tragedy The uninvited stranger started dancing on his own So we said goodbye to the glowing bride', 1, 18, '2019-01-27 03:01:58', '2019-01-27 03:01:58');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2014_10_12_000000_create_users_table', 1),
(5, '2014_10_12_100000_create_password_resets_table', 1),
(6, '2019_01_13_080116_create_posts_table', 1),
(7, '2019_01_17_145730_add_slug_to_posts', 1),
(8, '2019_01_24_062719_create_categories_table', 2),
(9, '2019_01_24_063002_add_category_id_to_posts', 2),
(10, '2019_01_25_123941_create_tags_table', 3),
(11, '2019_01_25_140633_create_post_tag_table', 4),
(12, '2019_01_27_054713_create_comments_table', 5),
(13, '2019_01_27_060232_create_comments_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `slug`, `category_id`, `created_at`, `updated_at`) VALUES
(5, 'Comets to Catch in 2019', 'We start the new year with a nice assortment of slightly used comets left over from 2018. Enjoy them while you can, as 2019 won\'t be as generous with its comet candy. While a dozen or more comets are visible in most years, the majority are on the dim side, little misty spots of magnitude 11 and fainter. In the magnitude 10.5 and brighter category — those within range of a 8-inch telescope from somewhat light-polluted skies — there are currently five easily accessible comets. Let\'s start with the brightest and work from there.', 'comets-to-catch', 1, '2019-01-20 07:16:57', '2019-01-20 07:16:57'),
(6, 'Astrophotography: Bits, Bytes & Dynamic Range', 'Before you can have a proper understanding of what it means to stretch your image, you need to be sure you understand what your image is made of: numbers. That’s it . . . lots and lots of numbers. Every pixel has a number that corresponds to how bright the pixel should appear. Small numbers are dark, and large numbers are bright. Monochrome (black and white) images have just one number per pixel; color images have three numbers, one each for the red, green, and blue contribution. Simple, right?\r\n\r\nBits of Data\r\nThe next thing to consider is how those numbers are represented by a computer. The answer to that is bits. The more bits, the larger the number you can represent. A single bit can be 0 or 1. Black or white. Not much variation in there. We would could call this extremely low dynamic range.\r\n\r\nA computer represents a number as a whole string of bits, like this:\r\n\r\n00101010\r\n\r\nThis is actually the number 42. The more bits or places you have, the more unique combinations of 1’s and 0’s are possible, and thus the more numbers can be represented. Essentially, every permutation of 1’s and 0’s is a unique numeric value. How this works is fascinating, but beyond the scope of an astrophotography blog. Note, I’m going to neglect negative numbers here, as well as something called “endianness” . . . I am not a sadist. This blog is not for people who are already engineers!\r\n\r\nTwo bits can store the numbers from 0 to 3, three bits can store the numbers from 0 to 7, and so on, with additional bits supporting exponentially larger numbers. It’s not terribly important that you understand this; just remember, more bits equals larger numbers . . . and lots more variation in tone values that can be recorded or stored.', 'Astrophotography', 1, '2019-01-20 07:17:48', '2019-01-20 07:17:48'),
(7, 'China’s Chang’e 4 Lands on the Lunar Farside', 'China completed a first in lunar exploration on Thursday, January 3rd, as its spacecraft Chang\'e 4 (named after the Moon goddess in Chinese lore) landed on the lunar farside.\r\n\r\nThe spacecraft touched down at lunar longitude 177.6°E, latitude 45.5°S in Von Kármán crater at 2:26 Universal Time (UT), as per China\'s University of Geosciences at Wuhan. The lander wasted no time getting straight to work, snapping images of the lunar terrain. The small Yutu 2 rover rolled down the ramp and started exploring Von Kármán crater on the same day at 14:22 UT', 'Chinas-Change-Lands', 2, '2019-01-20 07:19:50', '2019-01-20 07:19:50'),
(8, 'NASA Renews Interest in SETI', 'In July I wrote about innovative approaches for the Search for Extraterrestrial Intelligence (SETI). In that column I lamented the fact that NASA support for this field dried up in the 1990s and had not returned, even though astrobiology has since flourished. Many of us felt that the bureaucratically maintained distinction between astrobiology and SETI did not make intellectual sense, and we longed for SETI to be let in from the cold.\r\n\r\nSometimes wishes come true.\r\n\r\n\r\nWith the number of know exoplanets exploding (here, an artist\'s concept of an Earth-size planet in a binary star system), NASA\'s renewed interest in the hunt for signs of intelligent life elsewhere is exciting. \r\nNASA / JPL-Caltech\r\n\r\nAs that column went to press I received an email asking if I would help organize a workshop on “technosignatures.” The sponsor? NASA. That got my attention. The purpose was to explore how to best use NASA resources in a renewed search for extraterrestrial intelligence. Apparently, Congress’s new federal budget mandated that NASA spend $10 million “to search for technosignatures, such as radio transmissions, in order to meet the NASA objective to search for life’s origin, evolution, distribution, and future in the universe.” Wow!\r\n\r\nThe workshop, which took place in September, was highly stimulating, and given the renewed government interest in SETI, the mood was bright and optimistic. Along with evaluation of historical and current searches, there was an openness to new ideas born of a kind of humility. We can’t really second-guess the properties or motivations of technological aliens, so we have to cast a wide net. In addition to “traditional” SETI searches for radio signals or laser pulses, we must be alert to more passive signs of technological entities that might not be trying to get in touch with anyone. These include possible artifacts beyond or within our own solar system, or planetary atmospheres altered or engineered by industrial activities.\r\n\r\nAttendees made an effort to stick to the prosaic questions: What observing programs can we ramp up in the next few years using NASA’s current or expected assets and instruments? How can NASA best collaborate with private partners such as the SETI Institute and Breakthrough Listen?\r\n\r\nBut with SETI it’s hard to avoid deep philosophical musings. Some talks at the workshop delved into abstract but necessary puzzles about the properties and behavior of distant, advanced civilizations — even about what we mean by “advanced” and “civilization.” SETI has always combined solid engineering, daring speculation, and profound questioning.\r\n\r\nThis admixture didn’t always sit well with some. At the first international SETI conference in Byurakan, Soviet Armenia in 1971, organizers Carl Sagan and Iosif Shklovsky welcomed historians, philosophers, linguists, and social scientists along with the scientists. At the time, one young Soviet astrophysicist asked that the humanities be left out, stating he didn’t want to listen to “windbags.” A leading American physicist exclaimed, “To hell with philosophy! I came here to learn about observations and instruments . . .”\r\n\r\nThis historical tension seemed absent from September’s workshop. Although our prime directive was to guide NASA in the use of its assets to search for technosignatures, there was respectful discussion of the more esoteric and humanistic questions that are naturally evoked, and a recognition that a mature SETI program going forward will involve more than just telescopes and computer models. Out of this will come new calls for proposals to NASA, and then a new era of federally funded SETI research. May it be long and fruitful.', 'NASA-Renews-Interest-in-SETI', 1, '2019-01-20 07:20:45', '2019-01-20 07:20:45'),
(9, 'Space Missions to Watch in 2019', 'What a busy year it has been! In 2018 we saw 112 successful space launches worldwide from eight countries, the most launches we\'ve seen since 1990. SpaceX and China alone account for more than half of these, with 21 and 39 launches, respectively — a record for both. This past year we also saw Mars InSight arrive at the Red Planet, ESA\'s BepiColombo mission head to Mercury, the launches of NASA\'s exoplanet-hunting TESS mission and the Parker Solar Probe, and China\'s bid to make the first ever landing on the lunar farside.\r\n\r\nThere\'s far more to come. Here are the top space science missions we\'re watching in 2019.\r\n\r\nLunar Ambitions\r\nThe New Year will see a multi-national armada of spacecraft return to the Moon.\r\n\r\nChange 5\r\nAn artist\'s concept shows China\'s Chang\'e 5 sample return mission lifting off from the Moon. \r\nCNSA\r\n\r\nChina\'s Chang\'e 4 is set to make the first ever soft landing on the lunar farside in early January 2019, followed by the Chang\'e 5 a sample return mission that should to occur by late 2019. The mission is a follow-on to the Queqiao orbital relay and the Chang\'e 4 lander.\r\n\r\nKey to its success is China\'s heavy-lift Long March 5 rocket, which had problems with its first-stage engine during its second flight in July 2017. Chang\'e 5 weighs in at a hefty 18,100 pound (8,200 kilogram) launch mass, which includes the lander, launch system, and sample return capsule — more than double that of Chang\'e 4 — necessitating the larger carrier rocket.\r\n\r\nISRO rover\r\nThe Chandrayaan 2 rover on Earth. \r\nISRO\r\n\r\nMeanwhile, the Indian Space Research Organization is entering the lunar lander game in 2019 with its Chandrayaan 2 mission. This spacecraft will carry an orbiter, rover, and lander and is set to launch atop a Geosynchronous Launch Vehicle Mark III from the Satish Dhawan Space Center in India on January 30, 2019. Chandrayaan 2 will land near the Simpelius N and Manzinus C craters — the first soft landing for the south pole region of the Moon.\r\n\r\nThe Google Lunar X prize competition to land on the Moon unfortunately saw its March 31, 2018, deadline pass with no claimants. The prize featured an award of $20 million for the first team to land on the Moon, drive 500 meters across its surface, and return pictures. Nevertheless, five teams remain committed to the goal and have either secured or are working to secure launch contracts to head to the Moon in 2019.\r\n\r\n\r\n\r\nSpaceIL: This Israeli-based company plans to launch its Beresheet lunar lander (named for the first Hebrew word in the Book of Genesis) on a SpaceX Falcon 9 rocket on February 13, 2019. The spacecraft will take about two and a half months to reach the Moon, through a slow cycle of elliptical raising orbits.\r\nALINA: Germany\'s Autonomous Landing and Navigation module (ALINA) could bring two Audi lunar quattro rovers near the Apollo 17 Taurus-Littrow landing site in 2019. ALINA is operated by the Part-Time Scientists, and the company is working to secure a launch contract with SpaceX.\r\nMoon Express: The sole remaining U.S. entry in the Google Lunar X prize competition, Moon Express still intends to field two missions in 2019: the MX-1E lander and the Lunar Outpost MX-3 lander. The company plans to launch these missions on Rocket Lab\'s Electron rocket, which carried out several successful launches from the company\'s Mahia New Zealand launch complex in 2018. MX-1E will carry a small optical telescope, a laser retro-reflector, and a Celestis memorial container.\r\nTeam Indus: Headquartered in Bangalore, India, this company is also looking to secure a launch contract in 2019. Two other teams, Synergy Moon and Hakuto, are looking to piggyback with Team Indus.\r\nAnother possible Moonbound mission is the Goonhilly Lunar Pathfinder, which would dispense nanosatellites into lunar orbit. The UK wants to field the Lunar Pathfinder in orbit around the Moon in time for the 50th anniversary of Apollo 11, but there\'s no word on a launch date or a carrier for this mission.', 'Space-missions-in-2019', 2, '2019-01-20 07:21:48', '2019-01-20 07:21:48'),
(10, 'Start 2019 with the Quadrantid Meteor Shower', 'Let\'s make it two in a row! Conditions were excellent for last month\'s Geminid meteor shower and will be again for the upcoming Quadrantids. This annual shower is something of a black sheep among meteor watchers with its brief peak, frigid viewing conditions, and odd origin. While active from late December to mid-January, most of the the shower\'s material is concentrated in a thin band which Earth passes through perpendicularly, the reason for its short duration. We\'re in and out in a little more than 6 hours.\r\n\r\nBut if maximum happens when the radiant stands high for your location, get ready for excitement. Sightings of 100 \"Quads\" an hour aren\'t uncommon, a not too rare prize. I\'ve only seen one maximum, back in the 1980s. It was a bitter cold morning, but meteors popped out all over, making it one of the best showers I\'ve ever seen.\r\n\r\nBright Quadrantid meteor\r\nIt was the only Quadrantid meteor captured by Maryland skywatcher Mike Hankey during 10 hours of photography, but it\'s a doozy. The bucket of the Big Dipper appears at upper right. Shower members strike the atmosphere at \"medium\" speeds of around 40 kilometers per second.\r\n\r\nAccording to Robert Lunsford of the American Meteor Society the 2019 peak will occur around 2:30 UT on Friday, January 4th, ideal for skywatchers in Europe, North Africa, and far western Asia. For other parts of the world, we can expect about 25 meteors per hour, still a good showing. No Moon will damage the meteoric goods as it\'s a super-thin crescent that won\'t show up till dawn. Speaking of which, if you get up before dawn to take in the meteor shower, stick around. You might just get to see the 672-hour-old Moon (1.5 days before new) 45 minutes before sunrise about 5° high in the southeastern sky, 15° to the lower left of Jupiter. If your eastern horizon allows, look for –0.4-magnitude Mercury glimmering about 2½° below the Moon. Bring binoculars for assistance.\r\n\r\nThe Quadrantids are a Northern-Hemisphere-only experience, as the radiant lies below the handle of the Big Dipper in Boötes at declination +49°. Northward of +40.5° latitude, the radiant is circumpolar, so it never actually dips below the horizon. As the stars pivot about Polaris, the radiant scrapes along the northern horizon for hours. While a low radiant will absolutely reduce meteor sightings, it may also afford us the opportunity to see at least a little of the shower at its peak across North America.\r\n\r\nCold comfort\r\nThe author relaxes on a lawn chair under a sleeping bag while anticipating the next meteor during last month\'s Geminid shower. Bundled against the cold or dew, meteor-watching is one of astronomy’s most relaxing and rewarding activities.\r\nBob King\r\n\r\n2:30 UT translates to 9:30 p.m. Eastern Standard Time (8:30 Central; 7:30 Mountain, and 6:30 Pacific). If nothing else, skywatchers on Thursday evening (January 3rd) can watch for Quadrantid Earthgrazers by facing the northern sky during the evening hours. Earthgrazers are slow-moving meteors that launch upward from the horizon and flare for a long time before fading out. If you live in the wilds of northern Canada, north of about latitude 60° north, the radiant will stand at least 20° high in the northern sky even at nadir, making it easy to catch the peak that evening.\r\n\r\nFor those outside the narrow peak zone, you can observe the shower in stages, checking for Earthgrazers during the evening and then rising an hour or two before the start of dawn — between 4 to 6 a.m. local time — when the radiant stands highest in the northeastern sky. As always, there\'s no preferred direction in which to look, but avoid staring at the radiant, where meteors approach head on and sketch only short streaks. Facing about 90° away from the radiant provides a mix of short and longer, more dramatic trails.\r\n\r\nEarth takes aim\r\nNotice how steeply the orbit of 2003 EH1 and its meteor spawn are inclined to Earth\'s orbit. Coming in perpendicularly, Earth literally shoots through the densest stream of meteoroids in hours. Positions and data are shown for Jan. 4.0 UT.\r\nNASA / JPL Horizons\r\n\r\nMost meteor showers originate from comets, and it appears that the Quadrantids do, too. Meteor expert Peter Jenniskens found an excellent match between the ~2.9 kilometer-wide object 2003 EH1 and the meteor stream. While it\'s likely an \"extinct\" comet because it doesn\'t exhibit a fuzzy coma, the shower may have been spawned by the break-up of a larger comet into smaller pieces in the not-too-distant past, one of which became 2003 EH1.', 'Start-2019-with-Meteor-Shower', 2, '2019-01-20 07:22:37', '2019-01-20 07:22:37'),
(11, 'Rotating Gas in a Quasar’s Heart', 'In the last few months, astronomers working with the Gravity instrument on the Very Large Telescope Interferometer in Chile have released a series of impressive measurements. These results include preliminary data that show a long-sought gravitational effect on a star’s light as it passed the Milky Way’s central black hole. But the result I want to talk about here involves a much larger, more distant black hole, the 300-million-solar-mass leviathan that powers the brightest “nearby” quasar, 3C 273.\r\n\r\n(I put nearby in quotation marks because this active galaxy in the constellation Virgo lies so far away that its light has taken some 2 billion years to reach us.)\r\n\r\nQuasars essentially look like brilliant dots in the sky. They are galaxy cores that blaze brightly due to the hot gas their supermassive black holes chow down on and burp out as gigantic plasma jets. But thanks to an inventive approach by the Gravity Collaboration, 3C 273’s pinprick is now transformed into a map of gas moving right around the black hole, exploring a region we’ve heretofore only been able to probe indirectly with spectra and flickers of light.\r\n\r\nTaking full advantage of the freedom that having a dedicated black holes blog gives me (wahahaha), I’d like to take you on a deep-dive into the Gravity Collaboration’s observations of the gas around 3C 273’s black hole to explain the unique approach this work involved.\r\n\r\nShifting Images\r\nLike other quasars, 3C 273 has a region of hot gas zooming around in the black hole’s vicinity that’s called the broad-line region. The name comes from the shape of the gas’s spectral lines, which are smeared out. In normal circumstances, a spectral line is a narrow thing — a single wavelength. But when the gas moves, the spectral line shifts: to longer, redder wavelengths if the gas is moving away from us, and to bluer, shorter wavelengths if the motion is toward us. This Doppler effect is the same reason that an ambulance siren cascades through the sound spectrum as it races past you.\r\n\r\nIf you could combine all the notes you hear as the ambulance passes, you would hear a broad, multi-note sound, with the central note being the siren’s actual or “rest” frequency. The same thing happens with moving gas, creating a broadened spectral line.\r\n\r\nThe amount of smearing tells us how fast the gas is moving. Gas orbiting close to a black hole can move faster around the beast than gas farther out, just as the inner planets of the solar system orbit the Sun at a faster clip than the outer ones do. Based on the smear, astronomers infer that the BLR is one of the closest regions to the black hole that we can detect.\r\n\r\nObservers use the BLR as a diagnostic, indicating things like the black hole’s mass. But even though the BLR is central to quasar studies, astronomers don’t actually know what it looks like. Is it the inner part of a disk around the black hole? Is it a halo of whizzing clouds?\r\n\r\nPrevious studies have tackled this question by inferring the gas’s motion and size from spectral patterns or the travel time of light echoes across the region. Reporting in the November 29th Nature, the Gravity Collaboration has now taken a completely different approach to studying the BLR, using blurry images of the gas itself.\r\n\r\nVLT\r\nA view of the four 8.2-meter VLT Unit Telescopes at the Paranal Observatory in Chile.\r\nESO\r\n\r\nThe team paired up the VLT’s four scopes in six different ways. Each pair of telescopes is separated by a unique distance, and their combined data create an image with the same resolution you’d obtain using a telescope as wide as the scopes’ separation. Telescopes closer together see the big, broad-brush picture, while telescopes farther apart home in on finer detail.\r\n\r\nBut turbulence in the atmosphere wiggles the image a wee bit. Each telescope pair sees a slightly different shift in where the image’s center is, explains Gravity team member Jason Dexter (Max Planck Institute for Extraterrestrial Physics, Germany). The center’s location also changes depending on which wavelength the astronomers observe.\r\n\r\nInstead of taking these shifts as confusion that needs to be overcome, the researchers have used the complexity in their favor. The key is that in the case of the BLR’s gas, each wavelength corresponds to a redshift or blueshift caused by the gas’s velocity. By measuring how the image shifts at a series of different wavelengths, the astronomers could see where in the image there was stuff going at the speed and in the direction that corresponds to that Doppler shift. So even though Gravity only sees a blurry image, by tracking how the center of that blur changes position from wavelength to wavelength, the team can reconstruct how the blurred-out gas is moving around the black hole.\r\n\r\nEvidence of Rotation\r\nBLR geometry\r\nThis diagram shows the principle geometry of the broad-line region (BLR) of the quasar 3C 273. The individual clouds are distributed in a thick ring (green shaded area) and rotate around the central black hole. The astronomers on Earth view this system at a slight angle (i).\r\n© GRAVITY Collaboration\r\n\r\nThis charting reveals that one side of the BLR glow is moving toward us, the other away, just as you’d expect if the gas is rotating. The pattern matches what you might see if the gas inhabits a puffed-up disk, with clouds orbiting at a range of inclinations to our line of sight. Furthermore, the gas is rotating around the axis drawn by the black hole’s powerful jet, which is exactly what should happen if the gas is rotating around the black hole.\r\n\r\nNo one’s been able to clearly show this toward-and-away motion in the BLR’s spectra before. It’s been one of the great mysteries of this gas, Dexter says: Before, astronomers could only see a “featureless lump,” a big, fat emission line with no clear pattern in the gas’s motion. Gravity’s data prove the gas is indeed rotating.\r\n\r\nNow that they can see (if only vaguely) the structure, researchers can estimate how big the BLR is. The size, about 145 light-days, is within the range of previous estimates but on the small side.\r\n\r\nDetails aside, here’s the takeaway: We’re actually watching gas orbit a gargantuan black hole more than a billion light-years from Earth.\r\n\r\nWhy does this feat matter? The BLR enables astronomers to study what happens near supermassive black holes. The better we understand the gas’s motion and the size of the region it moves through, the better we’ll understand how these black holes feed and how they power quasars.', 'Rotating-Gas-in-a-Quasars-Heart', 2, '2019-01-20 07:23:26', '2019-01-20 07:23:26'),
(12, 'Assigning Categories', 'A narrative or story is a report of connected events, real or imaginary, presented in a sequence of written or spoken words, or still or moving images, or both. The word derives from the Latin verb narrare, \"to tell\", which is derived from the adjective gnarus, \"knowing\" or \"skilled\".', 'assigning-categories', 1, '2019-01-25 02:01:30', '2019-01-25 02:24:18'),
(14, 'Adding tag', 'Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.\r\n\r\nUnless the context is clear (as with the “Notifications” example, where it is understood that the “4” is the number of notifications), consider including additional context with a visually hidden piece of additional text.Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.\r\n\r\nUnless the context is clear (as with the “Notifications” example, where it is understood that the “4” is the number of notifications), consider including additional context with a visually hidden piece of additional text.Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.\r\n\r\nUnless the context is clear (as with the “Notifications” example, where it is understood that the “4” is the number of notifications), consider including additional context with a visually hidden piece of additional text.Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.\r\n\r\nUnless the context is clear (as with the “Notifications” example, where it is understood that the “4” is the number of notifications), consider including additional context with a visually hidden piece of additional text.', 'adding-tag-tutorial', 1, '2019-01-25 22:42:53', '2019-01-25 22:42:53'),
(15, 'Addin UI/UX', 'Note that depending on how they are used, badges may be confusing for users of screen readers and similar assistive technologies. While the styling of badges provides a visual cue as to their purpose, these users will simply be presented with the content of the badge. Depending on the specific situation, these badges may seem like random additional words or numbers at the end of a sentence, link, or button.\r\n\r\nUnless the context is clear (as with the “Notifications” example, where it is understood that the “4” is the number of notifications), consider including additional context with a visually hidden piece of additional text.', 'ui-ux-tuts', 1, '2019-01-25 22:45:06', '2019-01-25 22:45:06'),
(18, 'Dream theater', 'Several years ago\r\nIn a foreign town\r\nFar away from home\r\nI met the Count of Tuscany\r\nA young eccentric man\r\nBred from royal blood\r\nTook me for a ride\r\nAcross the open countryside\r\nGet into my car\r\nLet\'s go for drive\r\nI love the way I feel uptight\r\nJust step inside\r\nMaybe you\'ll recall\r\nI kind of felt curious\r\nA character inspired by my brother\'s life\r\nWinding through the hills\r\nSeeing far behind\r\nOn and on we drove\r\nDown narrow streets and dusty roads\r\nAnd last we came upon\r\nA picturesque estate\r\nOn sprawling emerald hills\r\nAn ancient world of times gone by\r\nNow let me introduce\r\nMy brother\r\nA bitter gentleman - historian\r\nSucking on his pipe\r\nDistinguished accent\r\nMaking me uptight - no accident\r\nI\r\nWant to stay alive\r\nEverything about this place just doesn\'t feel right\r\nI\r\nI don\'t wanna die\r\nSuddenly I\'m…', 'count-of-tuscany', 2, '2019-01-25 22:48:19', '2019-01-25 23:37:14'),
(19, 'java', 'asdsadsa ds dsjdsds dsjdsd ajkajsd ajks dka sd jsa dkj ads a sdjk asdjk asdjasd jjasdj jasdjasd.', 'java-tutorials', 6, '2019-01-28 00:07:50', '2019-01-28 00:07:50');

-- --------------------------------------------------------

--
-- Table structure for table `post_tag`
--

CREATE TABLE `post_tag` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `tag_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_tag`
--

INSERT INTO `post_tag` (`id`, `post_id`, `tag_id`) VALUES
(3, 14, 1),
(4, 14, 2),
(6, 14, 4),
(10, 18, 2),
(13, 11, 1),
(14, 19, 2),
(15, 19, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Heavymetal', '2019-01-25 08:51:09', '2019-01-26 06:17:43'),
(2, 'Marketing', '2019-01-25 08:52:27', '2019-01-26 02:06:41'),
(4, 'backend development', '2019-01-25 08:53:02', '2019-01-25 08:53:02'),
(5, 'marketing', '2019-01-27 01:20:31', '2019-01-27 01:20:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'basanta', 'basantalfc@gmail.com', NULL, '$2y$10$hsGirlBwwHsVFXILKRyk0eQAoF2NxCteGaX3P3SrgNsnLARPHe0La', 'xz8T6zHKOm0TXa5ICfkn2Dxeced6Gdxa4dulgdZs3pwRFhYKBQksHCZskypt', '2019-01-24 03:01:40', '2019-01-25 00:18:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_post_id_foreign` (`post_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_tag_post_id_foreign` (`post_id`),
  ADD KEY `post_tag_tag_id_foreign` (`tag_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `post_tag`
--
ALTER TABLE `post_tag`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `post_tag`
--
ALTER TABLE `post_tag`
  ADD CONSTRAINT `post_tag_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`),
  ADD CONSTRAINT `post_tag_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
