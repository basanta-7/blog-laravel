<?php

use Faker\Generator as Faker;
use  Illuminate\Support\Str;
use App\Category;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence,
        'body' => $faker->text,
        'slug' => str_slug($faker->sentence),
        'category_id' => Category::inRandomOrder()->first()->id
    ];
});
