<?php

use Faker\Generator as Faker;

$factory->define(App\SupportTicket::class, function (Faker $faker) {
    return [
        'question' => $faker->sentence(),
    ];
});
