<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $table = 'categories'; //manually telling to use categories table nagareni huncha
    
    public function posts()
    {
    	return $this->hasMany('App\Post');
    }
}
