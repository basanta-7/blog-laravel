<?php

	namespace app\Orders;
	use app\Billing\BankPaymentGateway;


	class OrderDetails{

		private $paymentGateway;

		public function __construct(BankPaymentGateway $bankPaymentGateway){
			$this->bankPaymentGateway = $bankPaymentGateway;
		}

		public function all(){
			$this->bankPaymentGateway->setDiscount(500);

			return [
				'name' => 'Victor Anichebe',
				'address' => '123 coders tape street'
			];
		}
	}
