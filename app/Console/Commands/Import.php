<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Import extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:users {user} {--queue}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'imports the user from a csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   
        // dd($this->arguments());
        // dd($this->options());
        // dd($this->getOutput()->isVerbose());
        $users = [
            0 =>['email' => 'danielbryan@gmail.com', 'name' => 'daniel bryan'],
            1 =>['email' => 'sethrollins@gmail.com', 'name' => 'seth rollins'],
            2 =>['email' => 'danielbryan@gmail.com', 'name' => 'daniel bryan'],
            3 =>['email' => 'sethrollins@gmail.com', 'name' => 'seth rollins'],
            4 =>['email' => 'danielbryan@gmail.com', 'name' => 'daniel bryan'],
            5 =>['email' => 'sethrollins@gmail.com', 'name' => 'seth rollins'],
            6 =>['email' => 'danielbryan@gmail.com', 'name' => 'daniel bryan'],
            7 =>['email' => 'sethrollins@gmail.com', 'name' => 'seth rollins'],
            8 =>['email' => 'danielbryan@gmail.com', 'name' => 'daniel bryan'],
            9 =>['email' => 'sethrollins@gmail.com', 'name' => 'seth rollins'],
        ];

        $bar =  $this->output->createProgressBar(count($users));

        foreach($users as $user){
            // save to database
            $this->info("saved user". $user['name']);
            $bar->advance();
        }
        $bar->finish();
        $this->info('Done !!');
    }
}
