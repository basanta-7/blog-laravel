<?php

namespace App\Http\Controllers;


use App\Jobs\SendEmailNewPostJob;
use App\Mail\NewPostCreated;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Post;
use App\Category;
use Session;
use App\Tag;

class PostController extends Controller
{
    /**
    * Display a listing of the resource.

     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        if($request->search){
            $posts = Post::orderby('created_at','desc')->where('title', 'like', '%' .$request->search . '%')->paginate(5);
        }
        else{
            $posts = Post::orderBy('created_at','desc')->paginate(5);
        }
        //create a variable and store all blog post in it
        //return view and pass variable of above
        return view('posts.index')->withPosts($posts);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tags = Tag::all();
        $all_tags=[];
        foreach ($tags as $tag){
            $all_tags[$tag->id] = $tag->name;
        }

        $categories = Category::all();
        return view('posts.create')->withCategories($categories)->withTags($all_tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate
        $this->validate($request,array('title'=> 'required|max:255',
                                            'body' => 'required',
                                            'slug' => 'required|alpha_dash|min:5|max:255|unique:posts,slug',
                                            'tags' => 'required'));
        //store in database
        $post = new Post;
        $post->title = $request->title;
        $post->body = $request->body;
        $post->category_id = $request->category_id;
        $post->slug = $request->slug;

        $post->save();

        $post->tags()->sync($request->tags,false);

        dispatch(new SendEmailNewPostJob())
                ->delay(Carbon::now()->addSeconds(5));

        Session::flash('success','The blog post was successfully saved !');

        //redirect to view
        return redirect()->route('posts.show',$post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);
        return view('posts.show')->with('post',$post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tags = Tag::all();
        $all_tags=[];
        foreach ($tags as $tag){
            $all_tags[$tag->id] = $tag->name;
        }

        $categories = Category::all();
        $cats = array();
        foreach ($categories as $category) {
            $cats[$category->id] = $category->name;
        }
        //find the post in view and store in var
        $post = Post::find($id);
        //return the view  and pass in var we previous;y created
        return view('posts.edit')->withPost($post)->withCategories($cats)->withTags($all_tags);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate data
        $post = Post::find($id);
        if($request->input('slug') == $post->slug)
        {
            $this->validate($request,array('title'=> 'required|max:255','body' => 'required'));
            //Save the data to database
        }
        else
        {
            $this->validate($request,array('title'=> 'required|max:255','body' => 'required', 'slug' => 'required|max: 255| min:5| alpha_dash|unique:posts,slug'));
        }
                    //Save the data to database

        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->slug = $request->input('slug');
        $post->category_id = $request->input('category_id');
        $post->save();

        $post ->tags()->sync($request->tags,true);

        // set the flash data with success message
        Session::flash('success','This post was successfully updated');

        //redirect with flash data to posts.show
        return redirect()->route('posts.show',$post->id);
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->tags()->detach();
        $post->delete();

        Session::flash('success','The Post Was Successfully Succeded');

        return redirect()->route('posts.index');
    }

    public function search(Request $request){
        try {
            $search = $request->search;
            if($search == ''){
                $posts = Post::orderby('created_at','desc')->limit(25)->get();
            }else{
                $posts = Post::orderby('created_at','desc')->where('title', 'like', '%' .$search . '%')->limit(25)->get();
            }

            $response = [];
            foreach($posts as $post){
                $response[] = array("value" => $post->id, "label" => $post->title);
            }
            return response()->json($response);
        } catch (\Exception $e) {
            return $e;
        }
    }
}
