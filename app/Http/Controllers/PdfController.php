<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;

class PdfController extends Controller
{
    public function index(){
    	$users = \App\User::all();
    	$pdf = PDF::loadView('pdf.index', compact('users'));
    	return $pdf->download('demo.pdf');
    }
    public function videos(){
    	$videos = \DB::connection('mysql2')
    					->select("SELECT lpn_video_collections.slug as VideoCollection, lpn_videos.slug as VideoName FROM lpn_video_collections INNER JOIN lpn_videos on lpn_videos.video_collection_id = lpn_video_collections.id WHERE lpn_video_collections.parliament_type ='hr'");
    					// ->join('lpn_videos', 'lpn_video_collections.id', '=', 'lpn_videos.video_collection_id')
    					// ->get();
    	$pdf = PDF::loadView('pdf.videos', compact('videos'));
    	return $pdf->download('demo.pdf');
    }
}
