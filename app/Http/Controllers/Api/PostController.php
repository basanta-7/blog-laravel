<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;
use app\Post;
use DB;

class PostController extends Controller
{
    public function index()
    {
    	$posts = DB::table('posts')->paginate(100);
        // $posts = auth()->user()->posts;
        return $posts;
    }
}
