<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Response;

class PostApiController extends Controller
{
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->get();
        return response()->json(['data' => $posts],200);
    }
}
