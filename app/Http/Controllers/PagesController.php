<?php
	namespace App\Http\Controllers; 	//you beling in here this folder ,app only and doesnt allow to go out to go out use use tag

	use Illuminate\Http\Request;
	use App\Post;
	use App\Mail\ContactMail;
	use App\Jobs\SendEmailJob;
	use Session;

	class PagesController extends Controller	{
		
		public function getIndex(){
			$posts = Post::orderBy('created_at','desc')->paginate(5);
			return view('pages/welcome')->withPosts($posts);
		}

		public function getAbout(){
			$fnme = 'Basanta';
			$lnme = 'Pandey';
			$full = $fnme." ".$lnme;
			$email = 'basantalfc@gmail.com';
			$data = [];
			$data['name'] = $full;
			$data['email'] = $email;
			return view('pages/about')->withData($data);
			// return view('pages/about')->withFullname($full)->withEmail($email);
		}

		public function getCsv(Request $request){
			return view('csv.index');			
		}

		public function getContact(){
			return view('pages/contact');	
		}
		public function postContact(Request $request)
		{
			$this->validate($request,['email' => 'required|email',
										'message' => 'min:10',
										'subject' => 'min:3']);

			$data = array('email' => $request->email,
					 'subject' => $request->subject,
					 'bodyMessage' => $request->message); 
			
					//  try{
					// 	\Mail::to('basantalfc@gmail.com')->send(new ContactMail($data));
					//  }
					//  catch(\exception $e){
					// 	 dd($e);
					//  }
		   	$emailJob = (new SendEmailJob($data))->delay(\Carbon\Carbon::now()->addSeconds(5));
		   	dispatch($emailJob);

			// Mail::send('emails.contact', $data,function($message) use ($data){
			// 	$message->from($data['email']);
			// 	$message->to('basantalfc@gmail.com');
			// 	$message->subject($data['subject']);
			// });

        Session::flash('success','This message was successfully sent');
			return redirect('/');
		}
	}	
?>