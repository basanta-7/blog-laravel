<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Login extends Component
{
	public $form = [
		'email' => '',
		'password' => '' 
	];

    public function render()
    {
        return view('livewire.login');
    }

    public function submit(){
    	$this->validate([
    		'form.email' => 'required|email',
    		'form.password' => 'required',
    	]);

    	$status = \Auth::attempt($this->form);
    	if($status){
	    	return redirect('livewire');
    	}else{
    		$this->addError('message', 'No credentials found.');
    		redirect()->back();
    	}
    }
}
