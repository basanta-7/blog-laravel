<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Str;

class Comments extends Component
{
    use WithPagination;

    protected $listeners = [
    	'fileUpload' => 'handleFileUpload',
    	'ticketSelected' //if we have same function and event name no need to add function
    ];

    public $image;
    public $name;
    public $email;
    public $comment;
    public $ticket_id;

    public function ticketSelected($ticket_id){
    	$this->ticket_id = $ticket_id;
    }

    public function handleFileUpload($imageData){
        $this->image = $imageData;
    }

    public function storeImage(){
        if(! $this->image){
            return null;
        }

        $img = ImageManagerStatic::make($this->image)->encode('jpg');
        $name = Str::random().'.jpg';
        Storage::disk('public')->put($name, $img);
        return $name;
    }

    // realtime validation
    public function updated($field){
        $this->validateOnly($field, ['comment' => 'required|max:100']);
    }

    public function addComment(){
        $this->validate([
            'name' => 'required|min:3',
            'email' => 'email|required',
            'comment' => 'required|max:500'
        ]);

        $image = $this->storeImage();

        \App\Comment::create([
            'name' => $this->name,
            'email' => $this->email,
            'comment' => $this->comment,
            'image' => $image,
            'post_id' => 1,
            'support_ticket_id' => $this->ticket_id,
            'approved' => 1
        ]); 
        session()->flash('message', 'Comment added successfully :D .');

        $this->comment = "";
        $this->email = "";
        $this->name = "";
        $this->image = "";
    }

    public function remove($id){
        $comment = \App\Comment::find($id);
        if($comment){
            Storage::disk('public')->delete($comment->image);
            $comment->delete();            
        }
        // $this->comments = $this->comments->where('id', '!=', $id);
        session()->flash('message', 'Comment deleted successfully.');
    }

    public function render()
    {
        return view('livewire.comments',['comments' => \App\Comment::where('support_ticket_id', $this->ticket_id)->orderBy('created_at','desc')->paginate(3)]);
    }
}
