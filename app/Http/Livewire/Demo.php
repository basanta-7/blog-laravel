<?php

namespace App\Http\Livewire;

use Livewire\Component;
use app\User;

class Demo extends Component
{
	public $isLoading = true;
	public $count = 2;
	public $data = [];

    public function render()
    {
    	$this->data = User::all();
    	$this->isLoading = false;
        return view('livewire.demo');
    }

    public function increment(){
    	$this->isLoading = false;
    	$this->count++;
    	$this->isLoading = true;
    }

    public function decrement(){
    	$this->isLoading = false;
    	$this->count--;
    	$this->isLoading = true;
    }
}
