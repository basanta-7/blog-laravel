<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Contact;
use Session;

class ContactLive extends Component
{
	public $contacts;
	public $name;
	public $email;
	public $country;
	public $gender;
	public $message;
	public $idBeingRemoved = null;

	public function mount(){
		$this->contacts = Contact::all();
	}

    public function render()
    {
        return view('livewire.contact-live');
    }

    public function resetValues(){
    	$this->name = '';
    	$this->email = '';
    	$this->country = '';
    	$this->gender = '';
    	$this->message = '';
    }

    public function confirmContactRemoval($id){
    	$this->idBeingRemoved = $id;
    	$this->dispatchBrowserEvent('show-delete-modal');
    }

    public function store(){
    	$this->validate([
            'name' => 'required|min:5',
            'email' => 'required|email:rfc,dns',
            'country' => 'required|max:50',
            'gender' => 'required|max:50',
            'message' => 'required|max:1000'
        ]);
    	Contact::create([
    		'name' => $this->name,
    		'email' => $this->email,
    		'country' => $this->country,
    		'gender' => $this->gender,
    		'message' => $this->message
    	]);
    	Session::flash('message','Your Message Successfully');
    	$this->resetValues();
		$this->contacts = Contact::orderBy('created_at', 'desc')->get();
    }

    public function destroy(){
    	if($idBeingRemoved){
    		$record = Contact::where('id', $idBeingRemoved);
            $record->delete();
    	}
    	Session::flash('message','Contact Deleted Successfully');
		$this->contacts = Contact::orderBy('created_at', 'desc')->get();
    }
}
