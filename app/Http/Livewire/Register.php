<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Register extends Component
{
	public $form = [
		'name' => '', 
		'email' => '', 
		'password' => '', 
		'password_confirmation' => '' 
	];

    public function render()
    {
        return view('livewire.register');
    }

    public function submit(){
    	$this->validate([
    		'form.email' => 'required|email',
    		'form.name' => 'required|max:20',
    		'form.password' => 'required|confirmed'
    	]);

    	\App\User::create($this->form);
    	return redirect(route('livewire-login'));
    }
}
