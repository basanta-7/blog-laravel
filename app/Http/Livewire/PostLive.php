<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithPagination;
use App\Post;
use App\Comment;
use Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Str;

class PostLive extends Component
{
    use WithPagination;

    protected $listeners = ['fileUpload' => 'handleFileUpload'];

    public $isLoading = false;
	public $post;
    public $newComment;
    public $newName;
    public $image;
    public $newEmail;
    // public $comments;
    public $initialComments;

    public function handleFileUpload($imageData){
        $this->image = $imageData;
    }

    public function storeImage(){
        if(! $this->image){
            return null;
        }

        $img = ImageManagerStatic::make($this->image)->encode('jpg');
        $name = Str::random().'.jpg';
        Storage::disk('public')->put($name, $img);
        return $name;
    }

    public function mount(){
    }

    public function render()
    {
        $this->isLoading = true;
		$this->post = Post::first();
        
        if($this->post){
            $comments = $this->post->comments()->orderBy('created_at','desc')->paginate(5);
        }

        $this->isLoading = false;
        return view('livewire.post-live', compact('comments'));	
    }

    // realtime validation
    public function updated($field){
        $this->validateOnly($field, ['newComment' => 'required|max:100']);
    }

    public function addComment(){
        $this->validate([
            'newName' => 'required|min:3',
            'newEmail' => 'email|required',
            'newComment' => 'required|max:500'
        ]);

        $image = $this->storeImage();
        $this->isLoading = true;
        Comment::create([
            'name' => $this->newName,
            'email' => $this->newEmail,
            'comment' => $this->newComment,
            'image' => $image,
            'post_id' => $this->post->id,
            'approved' => 1
        ]); 
        session()->flash('message', 'Comment added successfully :D .');

        $this->newComment = "";
        $this->newEmail = "";
        $this->newName = "";
        $this->image = "";
        $this->isLoading = false; 
    }

    public function remove($id){
        $comment = Comment::find($id);
        if($comment){
            Storage::disk('public')->delete($comment->image);
            $comment->delete();            
        }
        // $this->comments = $this->comments->where('id', '!=', $id);
        session()->flash('message', 'Comment deleted successfully.');
    }
}

