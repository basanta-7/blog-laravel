<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\User;

class Users extends Component
{
	public $perPage = 1;
    protected $listeners = [
        'load-more' => 'loadMore'
    ];

    
    public function loadMore()
    {
        $this->perPage = $this->perPage + 1;
    }

    public function render()
    {
        $users = User::latest()->paginate($this->perPage);
        $this->emit('userStore');
        return view('livewire.users',['users' => $users]);
    }
}
