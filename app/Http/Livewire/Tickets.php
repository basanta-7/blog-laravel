<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Tickets extends Component
{
	public $active;

	protected $listeners = ['ticketSelected'];

	public function ticketSelected($ticketId){
		$this->active = $ticketId;
	}

	public function mount(){
    	$this->active = \App\SupportTicket::first()->id;
	}

    public function render()
    {
        return view('livewire.tickets',[
        	'tickets' => \App\SupportTicket::all(),
        ]);
    }
}
