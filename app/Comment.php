<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['name', 'email', 'comment', 'image', 'approved', 'post_id', 'support_ticket_id']; 

    public function post()
    {
    	return $this->belongsTo('App\Post');
    }
    public function getImagePathAttribute(){
    	return \Storage::disk('public')->url($this->image);
    }
}
