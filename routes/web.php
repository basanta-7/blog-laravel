		<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['middleware' => ['web']], function () {
	Route::get('blog/{slug}', ['as' => 'blog.single', 'uses' => 'BlogController@getSingle'])->where('slug', '[\w\d\-\_]+');
	Route::get('blog', ['uses' => 'BlogController@getIndex', 'as' => 'blog.index']);
	Route::get('contact','PagesController@getContact');
	Route::get('about', 'PagesController@getAbout');
	Route::post('contact', 'PagesController@postContact');
	Route::get('/', 'PagesController@getIndex');
	Route::resource('posts', 'PostController');
		Route::resource('tags','TagsController',['except' => ['create']]);

		//comments
		Route::post('comments/{post_id}',['uses' =>'CommentsController@store', 'as' => 'comments.store']);
	//categories
	Route::resource('categories','CategoryController',['except' => ['create']]);
	Route::post('/search-posts','PostController@search')->name('search.posts');
	
});
Route::get('pdf','PdfController@index');
Route::get('parliament-videos-pdf','PdfController@videos');
Route::get('csv','PagesController@getCsv');
Route::get('pay','PayOrderController@store');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::view('users','users');


// livewire routes from here
// Route::get('livewire','HomeController@livewire');
// Route::view('livewire/contacts','cms-livewire.contact');

Route::livewire('/livewire', 'home')->middleware('auth');
Route::group(['middleware' => 'guest'], function(){
	Route::livewire('/livewire/login', 'login')->name('livewire-login');
	Route::livewire('/livewire/register', 'register');
});
